package com.zwt.springbootfanoutconsumer;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.concurrent.TimeUnit;

@EnableRabbit
@SpringBootApplication
public class SpringBootFanoutConsumerApplication {

    public static void main(String[] args) throws Exception{

        SpringApplication.run(SpringBootFanoutConsumerApplication.class, args);


        System.out.println("rabbit service startup");



//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringBootFanoutConsumerApplication.class);
//        System.out.println("rabbit service startup");
//        TimeUnit.SECONDS.sleep(60);
//        context.close();

    }


}
