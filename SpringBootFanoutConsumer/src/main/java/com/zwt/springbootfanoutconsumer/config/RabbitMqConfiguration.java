package com.zwt.springbootfanoutconsumer.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMqConfiguration {

    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("fanout_order_exchange", true, true);
    }


    @Bean
    public Queue DuanXinQueue() {
        return new Queue("DuanXin.fanout.queue");
    }

    @Bean
    public Queue emailQueue() {
        return new Queue("email.fanout.queue");
    }


    @Bean
    public Queue smsQueue() {
        return new Queue("sms.fanout.queue");
    }




    @Bean
    public Binding smsBinding() {
        return BindingBuilder.bind(smsQueue()).to(fanoutExchange());
    }

    @Bean
    public Binding DuanXinBinding() {
        return BindingBuilder.bind(DuanXinQueue()).to(fanoutExchange());
    }


    @Bean
    public Binding emailBinding() {
        return BindingBuilder.bind(emailQueue()).to(fanoutExchange());
    }

}
