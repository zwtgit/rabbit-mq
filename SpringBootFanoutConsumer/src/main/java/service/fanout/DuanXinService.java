package service.fanout;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

// bindings其实就是用来确定队列和交换机绑定关系
//@RabbitListener(bindings = @QueueBinding(
//        // email.fanout.queue 是队列名字，这个名字你可以自定随便定义。
//        value = @Queue(value = "DuanXin.fanout.queue", autoDelete = "false"),
//        // order.fanout 交换机的名字 必须和生产者保持一致
//        exchange = @Exchange(value = "fanout_order_exchange",
//                // 这里是确定的rabbitmq模式是：fanout 是以广播模式 、 发布订阅模式
//                type = ExchangeTypes.FANOUT)
//))
@Component
//@Service
public class DuanXinService {
    // @RabbitHandler 代表此方法是一个消息接收的方法。该不要有返回值
//    @RabbitHandler
    @RabbitListener(queues = {"DuanXin.fanout.queue"})
    public void messagerevice(String message) {
        // 此处省略发邮件的逻辑
        System.out.println("DuanXin-------------->" + message);
    }
}



//@Component
//public class MessageHandler {
//
//    @RabbitListener(queues = "zhihao.miao.order")
//    public void handleMessage(byte[] message){
//        System.out.println("消费消息");
//        System.out.println(new String(message));
//    }
//}