package com.zwt.simple;

import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @author: ML李嘉图
 * @description: Producer 简单队列生产者
 * @Date : 2021/3/2
 */
// 简单模式----消费者
public class Consumer {
    public static void main(String[] args) {
        // 所有的中间件技术都是基于tcp/ip协议基础上构建新型的协议规范,rabbitmq遵循的是amqp
        // 协议遵循 ip port

        // 1、创建连接工程
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 通过连接工厂设置账号密码
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        // 虚拟访问节点
        connectionFactory.setVirtualHost("/");
        Connection connection = null;
        Channel channel = null;
        try {
            // 2、创建连接connection
            connection = connectionFactory.newConnection("生产者");
            // 3、通过连接获取通道channel
            channel = connection.createChannel();
            // 4、 通过创建交换机、声明队列，绑定关系，路由key，发送消息和接受消息
            channel.basicConsume("queue1", true, new DeliverCallback() {
                public void handle(String consumerTag, Delivery message) throws IOException {
                    System.out.println("收到消息是：" + new String(message.getBody(), "UTF-8"));
                }
            }, new CancelCallback() {
                public void handle(String consumerTag) throws IOException {
                    System.out.println("接受消息失败了。。。。");
                }
            });
            System.out.println("开始接受消息");
            // 进行阻断，接受消息不关闭
            System.in.read();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 7、关闭通道（先关通道，再关连接）
            if(channel!=null && channel.isOpen()){
                try {
                    channel.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // 8、关闭连接
            if(connection!=null && connection.isOpen()){
                try {
                    connection.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
