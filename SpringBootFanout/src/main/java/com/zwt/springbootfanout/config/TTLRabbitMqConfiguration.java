package com.zwt.springbootfanout.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

@Configuration
public class TTLRabbitMqConfiguration {
    @Bean
    public DirectExchange ttlDirectExchange() {

        return new DirectExchange("ttl_direct_exchange", true, true);
    }


    @Bean
    public Queue tttDireclQueue() {

        HashMap<String, Integer> map = new HashMap<>();
        map.put("x-message-ttl",2000);
        return new Queue("ttl.direct.queue",true);
    }

    @Bean
    public Binding ttlDirectBinding() {
        return BindingBuilder.bind(tttDireclQueue()).to(ttlDirectExchange()).with("ttl");
    }
}
