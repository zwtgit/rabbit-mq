package com.zwt.springbootfanout;

import com.zwt.springbootfanout.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootFanoutApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootFanoutApplication.class, args);
    }

    @Test
    public void testMackOrderttl() {
        OrderService.makeOrder(1, 1, 12);
    }
}
